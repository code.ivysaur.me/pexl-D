# pexl-D

![](https://img.shields.io/badge/written%20in-C%2B%2B%2C%20x86%20assembly%2C%20Pexl-blue)

A statically typed, compiled programming language.

A backtracking LL(0) parser emits x86 assembly for nasm. A runtime library written in raw x86 assembly provides a reference-counted object system and a string library. 

Pexl is a high-level language, supporting first-class/higher-order functions, reference counting, and some type inference.

The compiler runs on Win32 and Linux to produce small (~2KB) fully contained executables (no libc dependency).

Tags: PL


## Download

- [⬇️ pexl-20150607.7z](dist-archive/pexl-20150607.7z) *(719.54 KiB)*
- [⬇️ pexl-20130401-bin.zip](dist-archive/pexl-20130401-bin.zip) *(1.33 MiB)*
